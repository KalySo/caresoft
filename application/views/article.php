<section class="white darken-4 col s8 m9 l10">
	<!-- Articles -->
				
				<div class="articles">
					<h4 class="center red-text text-darken-4">Les Articles </h4>
					<div class="row">
						<div class="col s12 m4 l4">
							<div class="card">
								<div class="card-image">
									<img src="<?php echo site_url('assets/image/computer.jpg'); ?>" alt="une image" class="responsive-img" style="height: 170px">
								</div>
								<div class="card-title center-align red-text text-darken-4 ">
									<h5>La vaccination des moins de 5 ans</h5>
								</div>
								<div class="card-content">
									<p>Lorem ipsum Lorem ipsum si dolor amet Lorem ipsum si dolor amet Lorem ipsum si dolor amet Lorem ipsum si dolor amet </p>
								</div>

								<div class="card-action">
									<a href="#" class=""><i class="material-icons right" >edit</i></a>
									<a href="#" class=" "><i class="material-icons right">delete</i></a>
								</div>

							</div>
						</div>

						<div class="col s12 m4 l4">
							<div class="card">
								<div class="card-image">
									<img src="<?php echo site_url('assets/image/computer.jpg'); ?>" alt="une image" class="responsive-img">
								</div>
								<div class="card-title center-align red-text text-darken-4 ">
									<h5>Les maladies veneriennes chez les nouveaux nés</h5>
								</div>
								<div class="card-content">
									<p>Lorem ipsum Lorem ipsum si dolor amet Lorem ipsum si dolor amet Lorem ipsum si dolor amet Lorem ipsum si dolor amet </p>
								</div>

								<div class="card-action">
									<a href="#" class=""><i class="material-icons right" >edit</i></a>
									<a href="#" class=" "><i class="material-icons right">delete</i></a>
								</div>

							</div>
						</div>

					</div>

					</div>

					<!-- Le bouton plus -->
						<div class="fixed-action-btn">
			  				<a class="btn-floating btn-large red darken-4" href="<?php echo site_url('AdminController/addPersonnel'); ?>">
			    				<i class="large material-icons" style="color: white">add</i>
			  				</a>
						</div>	
				</div>  
</section>