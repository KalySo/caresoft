<link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/css/style.css'); ?>">
		<!-- L'image de garde -->
		<div class="overlay-container" id="accueil">
			<div class="overlay">
				
				<div class="mot">
					<br>
					<br>
					<br>
					<br>
					<h2 class="center light white-text text-darken-4 "><?php echo $cab->motBienvenue ?><br>
					</h2>
				</div>
				<!-- Ouvert tous les jours 24h/24 <br>
						Votre sante est notre priorite-->
				<br>
				<div class="container">
					<div class="row">
						<div class="col s12 offset-m4 m6"><a href="#" class="btn-large center red darken-4 waves-effect waves-light ">Je prends rendez-vous ! </a></div>
					</div>
				
				</div>

			<!-- Barre de recherche -->
			<!--	<div class="search">
					<br>
					<form class="container">
						<div class="input-field center">
							<input id="search" type="search" required>
							<label for="search" class="white-text text-darken-4 ">Rechercher ici...</label>
							<i class="material-icons right prefix" style="color: white">search</i>
							<i class="material-icons prefix right">close</i>
						</div>
					</form>
				</div>
			</div>  -->
		</div>
	</header>

	<section>
		<!-- Nos services  -->
		<div class="service-container container section" id="services" >
			<br><br>
			<div class="row">
				<h4 class="red-text text-darken-4 center">Les services que nous vous offrons... </h4>
				<br>

				<!-- Lecture des services en bd -->
				<?php foreach ($serv as $servs): ?>
				<div class="col s12 m4 l4">
					<div class="card">
						<div class="card-image">
							<img src="<?php echo site_url('assets/image/computer.jpg'); ?>" alt="une image" class="responsive-img">
						</div>
						<div class="card-title center-align red-text text-darken-4 ">
							<h5><?php echo $servs->nomService ?></h5>
						</div>
						<div class="card-content">
							<p><?php echo $servs->descService ?> </p>
							</div>
							
						</div>
				</div>
				<?php endforeach; ?>
			</div>

			<div class="row">
				<div class="col s12 m12 right-align">
					<a href="#" class=" red-text text-darken-4" style="border: 1px solid transparent; border-radius: 2px">
						<i class="material-icons prefix right">add</i> Voir plus </a>
				</div>
			</div>

			<div class="container">
				<div class="row">
					<div class="col s12 offset-m4 m6"><a href="#" class="btn-large center red darken-4 waves-effect waves-light ">Je prends rendez-vous ! </a></div>
				</div>
				
			</div>
			</div>
		</div>

		<!-- Le personnel -->
		<br>
		<div class="personnel-container grey lighten-4 " id="equipe">
		<br><br>
			<div class="container">
				<div class="row">
					<h4 class="center red-text text-darken-4">Découvrez les spécialistes qui vous accompagnent...</h4>
					<br>
					<div class="col s12 m4">
						<div class="card-panel">
								<h5 class="center">Albert FOKA</h5>
								<p class="center-align">Medecine Générale<br>
								Infirmier Supérieur<br>
								De  Lundi a Vendredi: 7h30-11h <br> 
								Samedi et Dimanche: 17h - 20h30
							</p>
						</div>
					</div>

					<div class="col s12 m4">
						<div class="card-panel">
								<h5 class="center">Sorelle KANA</h5>
								<p class="center-align">Medecine Générale<br>
									Cardiologue
								<br>
								De  Lundi a Vendredi: 7h30-11h <br> 
								Samedi et Dimanche: 17h - 20h30
							</p>
						</div>
					</div>

					<div class="col s12 m4">
						<div class="card-panel">
								<h5 class="center">Paul ATANGANA</h5>
								<p class="center-align">Medecine Générale<br>
								Chirurgien - dentiste<br>
								De  Lundi a Vendredi: 7h30-11h <br> 
								Samedi et Dimanche: 17h - 20h30
							</p>
						</div>
					</div>	
				</div>
			</div>

		</div>

		<!-- UNE IMAGE pour faire le parallax -->
			<div class="" style="background-image: url(<?php echo base_url('assets/image/medocs.jpg'); ?>); height: 400px ; background-size: cover;  background-attachment: fixed;background-position: center; background-repeat: no-repeat; margin-top:-20px ;">
				<div class="container">
					<div class="row">
						<div class="col s12 m12"><a href="#" class="center varlign red-text text-darken-4 "> <i class="material-icons prefix left">event_note</i>Voir nos campagnes</a>
						</div>
					</div>
				</div>	 
			</div>


		<!-- A propos -->
		
		<div class="section about-container" id="about">
			<br><br>
			<div class="container">
				<div class="row">
					<h4 class="center red-text text-darken-4">A propos de nous... </h4>
					<br>
					<div class="col s12 m6 ">
						<img src="<?php echo base_url('assets/image/medocs.jpg'); ?>" alt="une image" class="responsive-img">
					</div>

					
					<div class="col s12 m6 ">
					<!--	<ul class="tabs tabs-tranparent" style=".tabs .indicator: red;">
							<li class="tab"><a href="#histoire">Histoire</a></li>
							<li class="tab"><a href="#vision">Vision</a></li>
						</ul>
					-->
						<!-- Pour lire l'histoire et la vision du centre -->
					
							<h6 class="red-text text-darken-4">Notre Vision </h6>
							<p style="text-align: justify;"><?php echo $cab->vision ?></p>

							<h6  class="red-text text-darken-4">Notre Histoire </h6>
							<p style="text-align: justify;"><?php echo $cab->histoire ?></p>
							<br><br>


						<div class="container">
							<div class="row">
								<div class="col s12 m12"><a href="#" class="center-align red-text text-darken-4 "> <i class="material-icons prefix left">collections</i>Accéder a notre galerie </a></div>
							</div>
				
						</div>
					</div>


				</div>
			</div>
		</div>

		<!-- Les témoignages  -->
		
		<div class="section temoignage-container grey lighten-4" id="temoignage">
			<br><br>
			<div class="container">
				<div class="row">
					<h4 class="center red-text text-darken-4">Ce qu'ils en pensent...</h4>
					<br>
					<!-- Tests sur l'affichage -->
					<?php foreach ($tem as $tems): ?>
						
					<div class="col s12 m4">
						<div class="card-panel">
							<h6 style="font-weight: bold" class="center-align"><?php echo $tems->nomTem ?></h6>
							<p style="font-style: italic; " class="center-align light">
								" <?php 
									$tr = $tems->contenuTem;
									for ($i=0; $i < 60; $i++) { 
										echo $tr[$i];
									}
									echo "..."; 
								?> "
								<a href="#" class="red-text text-darken-4 ">plus</a>
							</p>
						</div>
					</div>
					<?php endforeach;?>
					<div class="row">
						<div class="col s12 m12 right-align">
							<a href="#" class=" red-text text-darken-4" style="border: 1px solid transparent; border-radius: 2px">
							<i class="material-icons prefix right">add</i> Voir plus </a>
						</div>
					</div>
					
				</div>
			</div>

			<div class="container">
				<div class="row">
					<div class="col s12 offset-m4 m6"><a href="#" class="btn-large center red darken-4 waves-effect waves-light ">Je prends rendez-vous ! </a></div>
				</div>
				
			</div>
		</div>
		<!-- Le Carousel  -->
		<div class="row hide-on-med-and-down">
			<div class="carousel carousel-slider center">
    			<div class="carousel-fixed-item center">
    				<h4 class="center white-text text-darken-4">Notre Galerie</h4>
     				<a href="#" class=" white-text text-darken-4">Aller a la Galerie
						<i class="material-icons prefix ">arrow_forward</i>  </a>
    			</div>
    			<div class="carousel-item  white-text" href="#one!" style="background-image: url(<?php echo base_url('assets/image/medocs.jpg'); ?>); background-size: cover; ">
    				<div class="carousel-container" style="background-color: rgba(51,51,51,0.7); width: 100%; height: 100%; border:1px red black; ">
    					<br>
    					<h2 >Des locaux propres</h2>
      					<a href="#" class="right white-text"><i class="material-icons"  style="font-size: 4em">chevron_right</i></a>
    				</div>	
    			</div>
    			<div class="carousel-item  white-text" href="#one!" style="background-image: url(<?php echo base_url('assets/image/garde.jpg'); ?>); background-size: cover; border:1px solid black">
    				<div class="carousel-container" style="background-color: rgba(51,51,51,0.7); width: 100%; height: 100%; border:1px red black; ">
    					<br>
    					<h2>Un personnel qualifié et acceuillant</h2>
      					<a href="#" class="right white-text"><i class="material-icons"  style="font-size: 4em">chevron_right</i></a>
    				</div>	
    			</div>
    			<div class="carousel-item  white-text" href="#one!" style="background-image: url(<?php echo base_url('assets/image/computer.jpg'); ?>); background-size: cover; ">
    				<div class="carousel-container" style="background-color: rgba(51,51,51,0.7); width: 100%; height: 100%; border:1px red black; ">
    					<br>
    					<h2>Des équipements abondants et sophistiqué</h2>
      					<a href="#" class="right white-text"><i class="material-icons"  style="font-size: 4em">chevron_right</i></a>
    				</div>	
    			</div>
    			<div class="carousel-item  white-text" href="#one!" style="background-image: url(<?php echo base_url('assets/image/heart.jpg'); ?>); background-size: cover; ">
    				<div class="carousel-container" style="background-color: rgba(51,51,51,0.7); width: 100%; height: 100%; border:1px red black; ">
    					<br>
    					<h2>Un suivi impéccable</h2>
      					<a href="#" class="right white-text"><i class="material-icons"  style="font-size: 4em">chevron_right</i></a>
    				</div>	
    			</div>
    		</div>
		
	</section>
	
	<script src="<?php echo site_url("assets/js/materialize.js") ?>" ></script>
	<script src="<?php echo site_url("assets/js/jquery.js") ?>" ></script>
	
	<script>
		document.addEventListener('DOMContentLoaded', function() {
    		var elems = document.querySelectorAll('.carousel');
    		var instances = M.Carousel.init(elems);
  		});
  
	</script>

	
