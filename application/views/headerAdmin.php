<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
<meta charset="utf-8">

<head>
	<title>Administrateur</title>
	<!-- Import du css -->
	<link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/css/materialize.min.css'); ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/css/style.css'); ?>">


	
	<!-- Import des material icon  -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

	<!-- Optimisation sur mobile -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body>
	<header class="col s8 m9 l10">
		<!-- Le nav -->
			<nav>
				<div class="nav-wrapper red darken-4">
					<a href="<?php echo site_url('AdminController/indexAdmin'); ?>" class="brand-logo">La Grace</a>
					<ul class="right">
						<li><a href="<?php echo site_url('AdminController/index'); ?>" >Mon espace</a></li>
					</ul>
				</div>
				
			</nav>
		
	</header>

	