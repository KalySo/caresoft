<section class="white darken-4 col s8 m9 l10">
	<form action="" method="post">
	    <div class="input-field">
			<input  id="nomCab" name="nomCab" value="<?php echo  $cab->nomCab; ?>">
			<label for="nomCab" class="active"> Nom du Cabinet</label>								
		</div>
		<br>
		<div class="input-field">
			<input name="histoire" id="histoire" value="<?php echo  $cab->histoire; ?>">
			<label for="histoire" class="active">Histoire du Cabinet</label>
													
		</div>
		<br>
		<div class="input-field">
			<input name="vision" id="vision" value="<?php echo  $cab->vision; ?>">
			<label for="vision" class="active"> Vision du Cabinet</label>
													
		</div>
		<br>
		<div class="input-field">
			<input name="motBienvenue" id="motBienvenue" value="<?php echo  $cab->motBienvenue; ?>">
			<label for="motBienvenue" class="active"> Mot de bienvenue du site</label>
													
		</div>
		<br>
		<div class="input-field">
			<div class="row">
				<div class="col s12 center">
					<a type="submit" class="btn red darken-4 waves-effect waves-light" href="<?php echo site_url('AdminController/updateCab/'.$cab->idCab); ?>">Enregistrer</a>
				</div>
			</div>
		</div>
    </form>
</section>
<!-- Du js et du jquery -->
<script src="<?php echo site_url('assets/js/jquery.js'); ?>"></script>
	<script src="<?php echo site_url('assets/js/materialize.js'); ?>"></script>
	<script type="text/javascript">
		document.addEventListener('DOMContentLoaded', function() {
    	var elem = document.querySelectorAll('.modal');
    	var instances = M.Modal.init(elem);
  	});

	</script>
</body>


</html>
