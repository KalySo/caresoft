<section class="white darken-4 col s8 m9 l10">
	<!-- Les témoignages  -->
				
			<div class="personel">
					<h4 class="center red-text text-darken-4">Les témoignages  </h4>
					<div class="row">
						<?php foreach ($tem as $tems): ?>
						<div class="col s12 m4 l4">
							 
							<div class="card-panel">
								<div class="card-content">
									<h6 style="font-weight: bold" class="center-align"><?php echo $tems->nomTem?></h6>
									<p style="font-style: italic" class="center-align light">"<?php echo $tems->contenuTem?>"</p>
								</div>

								<div class="card-action">
									<a href="#" class="modal-trigger" onclick="document.getElementById('modal1').style.display='block'"><i class="material-icons right" >edit</i></a>
									<a href="#" class="modal-trigger" onclick="document.getElementById('modal2').style.display='block'"><i class="material-icons right">delete</i></a>
								</div>


								<!-- Modal Edit-->
								<div id="modal1" class="mod modalEdit">
									<div class="modal-content">
										<div class="col s12 m12 l12 grey lighten-4">
											<a class="close right" onclick="document.getElementById('modal1').style.display='none'">&times;</a>
      										<br>
      										<form action="" method="post">
	      										<div class="input-field">
													<input class="validate" type="text" id="temoinName" name="temoinName" value="<?php echo $tems->nomTem?>">
													<label for="temoinName"> Nom</label>
													<span class="helper-text" data-error="Veuillez remplir ce champ" data-success="Validé"></span>
												</div>
												<br>
												<div class="input-field">
													<input class="validate" type="text" id="temoignage" name="temoignage" value="<?php echo $tems->contenuTem?>">
													<label for="temoignage"> Témoigmage</label>
													<span class="helper-text" data-error="Veuillez remplir ce champ" data-success="Validé"></span>
												</div>
												<br>
												<div class="input-field">
													<div class="row">
														<div class="col s12 center">
															<a type="submit" class="btn red darken-4 waves-effect waves-light" href="<?php echo site_url('AdminController/updateTem/'.$tems->idTem); ?>">Enregistrer</a>
														</div>
													</div>
												</div>
      										</form>
											
										</div>
									</div>
									
								</div>

								<!-- Modal Supprimer -->
								<div id="modal2" class="modalSuppr mod">
									<div class="modal-content">
										<div class="col s12 m12 l12 grey lighten-4">
											<a class="close right" onclick="document.getElementById('modal2').style.display='none'">&times;</a>
      										<br><br>
											<div class="input-field">
												<p class="center-align">Voulez-vous continuer la suppression ? </p>
											</div>
											<br>
											<div class="input-field">
												<div class="row">
													<div class="col s12 center">
														<button type="submit" class="btn red darken-4 waves-effect waves-light" onclick="document.getElementById('modal2').style.display='none'">Annuler</button>
														<a type="submit" class="btn green darken-4 waves-effect waves-light" href="<?php echo site_url('AdminController/deleteTem/'.$tems->idTem); ?>">Continuer</a>
													</div>
												</div>
											</div>
										</div>
									</div>
									
								</div>
							</div>
						</div>
						<?php endforeach;?>
					</div>

					<!-- Le bouton plus -->
					<div class="fixed-action-btn">
			  			<a class="btn-floating btn-large red darken-4" href="<?php echo site_url('AdminController/editTem'); ?>">
			    			<i class="large material-icons" style="color: white">add</i>
			  			</a>
					</div>	
			</div>

</section>
</div>
</section>
	<script src="<?php echo site_url('assets/js/jquery.js'); ?>"></script>
	<script src="<?php echo site_url('assets/js/materialize.js'); ?>"></script>
	<script type="text/javascript">
		document.addEventListener('DOMContentLoaded', function() {
    	var elem = document.querySelectorAll('.modal');
    	var instances = M.Modal.init(elem);
  	});

	</script>
</body>
</html>
