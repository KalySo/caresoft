<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<DOCTYPE html>
<html style="scroll-behavior: smooth;">
<head>
	<title>CareSoft</title>
	<!-- Import du css -->
	<link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/css/materialize.css'); ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/style.css">
	<link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/css/w3css.css'); ?>">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

	<!-- Import du JS -->
	<script type="text/javascript" src></script>

	<!-- Import des material icon  -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

	<!-- Optimisation sur mobile -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	
</head>
<body>
	<header>
		<!-- Le nav -->
		<div class="navbar-fixed ">
			<nav>
				<div class="nav-wrapper red darken-4">
					<a href="#" class="brand-logo"><?php echo $cabNom->nomCab; ?></a>
					<ul class="right">
						<li><a href="#services">Nos services</a></li>
						<li><a href="#equipe" >Personnel</a></li>
						<li><a href="#about">A propos</a></li>
						<li><a href="#temoignage">temoignages</a></li>
						<li><a href="#">Campagnes</a></li>
						<li><a href="#">Galerie</a></li>
						<li><a href="#">Blog</a></li>
						<li><a href="<?php echo site_url('Welcome/login'); ?>">Connexion</a></li>
					</ul>
				</div>
				
			</nav>
		</div>