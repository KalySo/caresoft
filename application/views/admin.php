<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
	<section class="admin-container">
		<div class="row admin">
			<!-- Le header (menu de navigation )-->
			<header class="col s4 m3 l2 grey lighten-4" >
				<div class="row">
					<!-- Le username -->
					<div class="username center">
						<i class="material-icons">person_pin</i>
						<br>
						<p class="center grey-text text-darken-1">Hi <?php echo $this->session->userdata('username'); ?> !</p>
					</div>

					<!-- Le menu -->
					<div class="collection grey lighten-4">
					
        				<a href="<?php echo site_url('AdminController/listArt'); ?>" class="collection-item grey-text waves-effect waves-dark"><i class="material-icons left">storage</i>Articles</a>
        				<a href="<?php echo site_url('AdminController/listService'); ?>" class="collection-item grey-text waves-effect waves-dark"><i class="material-icons left">group_work</i>Services</a>
       					<a href="<?php echo site_url('AdminController/listPersonnel'); ?>" class="collection-item grey-text waves-effect waves-dark"><i class="material-icons left">group</i>Equipe</a>
        				<a href="<?php echo site_url('AdminController/listTem'); ?>" class="collection-item grey-text waves-effect waves-dark"><i class="material-icons left">thumbs_up_down</i>Temoignage</a>
        				<a href="<?php echo site_url('AdminController/listrdv'); ?>" class="collection-item grey-text waves-effect waves-dark"><i class="material-icons left">event_note</i>Rendez-vous</a>
        				<a href="<?php echo site_url('AdminController/listCamp'); ?>" class="collection-item grey-text waves-effect waves-dark"><i class="material-icons left">event</i>Campagne</a>
        				<a href="<?php echo site_url('AdminController/listService'); ?>" class="collection-item grey-text waves-effect waves-dark"><i class="material-icons left">collections</i>Galerie</a>
        				<!-- group_add (material-icons)-->
      				</div>

      				<br><br><br>
      				<!-- Bouton de déconnexion -->
      				<div class="center">

      					<a href="<?php echo site_url('AdminController/deconnect'); ?>" class="btn waves-effect waves-light red darken-4">Deconnexion <i class="material-icons left">exit_to_app</i></a>
      				</div>

				</div>
			</header>
	

		



