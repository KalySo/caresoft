<section class="white darken-4 col s8 m9 l10">

				<!-- Les rendez-vous -->
			<div class="rdv">
					<h4 class="center red-text text-darken-4">La liste des rendez-vous  </h4>
					<div class="row">
						<table>
							<tr>
								<th>Num</th>
								<th>Date et Heure</th>
								<th>Motif</th>
								<th>Dr Consultant</th>
								<th>Patient</th>
							</tr>
							<tr>
								<td>1</td>
								<td>12/04/2019</td>
								<td>Consultation pour dysfonctionnement sexuel</td>
								<td>Albert FOKA</td>
								<td>Sorelle KANA</td>
							</tr>
							<tr>
								<td>2</td>
								<td>15/05/2019</td>
								<td>Consultation pour problemes des yeux</td>
								<td>Albert FOKA</td>
								<td>Sorelle KANA</td>
							</tr>
							<tr>
								<td>3</td>
								<td>23/04/2019</td>
								<td>Consultion pour médécine génerale</td>
								<td>Albert FOKA</td>
								<td>Sorelle KANA</td>
							</tr>
						</table> 
					</div>
				</div>
</section>