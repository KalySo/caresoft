<section class="white darken-4 col s8 m9 l10">
			<!-- Le personnel -->
				
				<div class="personel">
					<h4 class="center red-text text-darken-4">Le personel </h4>
					<div class="row">
						<div class="col s12 m4 l4">
							<div class="card-panel">
								<div class="card-content">
									<h5 class="center">Sorelle KANA</h5>
									<p class="center-align">Medecine Générale<br>
									Cardiologue
									<br>
								De  Lundi a Vendredi: 7h30-11h <br> 
								Samedi et Dimanche: 17h - 20h30
									</p>
								</div>

								<div class="card-action">
									<br>
									<a href="#" class=""><i class="material-icons right" >edit</i></a>
									<a href="#" class=" "><i class="material-icons right">delete</i></a>
								</div>

							</div>
						</div>

						<div class="col s12 m4 l4">
							<div class="card-panel">
								<h5 class="center">Paul ATANGANA</h5>
								<p class="center-align">Medecine Générale<br>
									Cardiologue
								<br>
								De  Lundi a Vendredi: 7h30-11h <br> 
								Samedi et Dimanche: 17h - 20h30
								</p>

								<div class="card-action">
									<br>
									<a href="#" class=""><i class="material-icons right" >edit</i></a>
									<a href="#" class=" "><i class="material-icons right">delete</i></a>
								</div>

							</div>
						</div>

						


						<!-- Le bouton plus -->
						<div class="fixed-action-btn">
			  				<a class="btn-floating btn-large red darken-4" href="<?php echo site_url('AdminController/addPersonnel'); ?>">
			    				<i class="large material-icons" style="color: white">add</i>
			  				</a>
						</div>

					</div>	
				</div>


</section>