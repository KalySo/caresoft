<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<meta charset="utf-8">
<html>
	<head>
		<title>Notre Blog</title>
		<!-- Import du css -->
	<link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/css/materialize.css'); ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/css/style.css'); ?>">


	
	<!-- Import des material icon  -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

	<!-- Optimisation sur mobile -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	</head>
	<body>
		
		<section class="blog">
			<h4 class="red-text darken-4 center">Notre blog</h4>
			<div class="container">
				<nav class="transparent center">
					<ul class="">
						<li><a href="" class="grey-text">argent </a></li>
						<li><a href="" class="grey-text">psycho </a></li>
						<li><a href="" class="grey-text">thérapie </a></li>
						<li><a href="" class="grey-text">MST </a></li>
					</ul>			
				</nav>
			</div>
		</section>
	</body>
</html>