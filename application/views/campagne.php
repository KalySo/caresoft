<section class="white darken-4 col s8 m9 l10">
	<!-- Les campagnes  -->
				
			<div class="campagnes">
					<h4 class="center red-text text-darken-4">Les Campagnes en cours...  </h4>
					<br>
					<div class="row">
						<div class="col s12 offset-l1 l10 m12">
							<div class="card horizontal">
								<div class="card-image">
									<img src="<?php echo site_url('assets/image/computer.jpg'); ?>" alt="une image" class="responsive-img" >
									<h5 class="red-text text-darken-4 center-align">Date</h5>
								</div>
								
								<div class="card-stacked">
								<div class="card-title center-align red-text text-darken-4 ">
									<h5>Campage de Vaccination contre le paludisme</h5>
								</div>
									<div class="card-content">
										<p>Lorem ipsum Lorem ipsum si dolor amet Lorem ipsum si dolor amet Lorem ipsum si dolor amet Lorem ipsum si dolor amet
										Lorem ipsum Lorem ipsum si dolor amet Lorem ipsum si dolor amet Lorem ipsum si dolor amet Lorem ipsum si dolor amet 
										Lorem ipsum Lorem ipsum si dolor amet Lorem ipsum si dolor amet Lorem ipsum si dolor amet Lorem ipsum si dolor amet  </p>
									</div>

									<div class="card-action">
									<a href="#" class=""><i class="material-icons right" >edit</i></a>
									<a href="#" class=" "><i class="material-icons right">delete</i></a>
								</div>
								</div>
							</div>
						</div>
						<div class="col s12 offset-l1 l10 m12">
							<div class="card horizontal">
								<div class="card-image">
									<img src="<?php echo site_url('assets/image/computer.jpg'); ?>" alt="une image" class="responsive-img">
									<h5 class="red-text text-darken-4 center-align">Date</h5>
								</div>
								
								<div class="card-stacked">
								<div class="card-title center-align red-text text-darken-4 ">
									<h5>Campage de Vaccination contre le paludisme</h5>
								</div>
									<div class="card-content">
										<p>Lorem ipsum Lorem ipsum si dolor amet Lorem ipsum si dolor amet Lorem ipsum si dolor amet Lorem ipsum si dolor amet
										Lorem ipsum Lorem ipsum si dolor amet Lorem ipsum si dolor amet Lorem ipsum si dolor amet Lorem ipsum si dolor amet 
										Lorem ipsum Lorem ipsum si dolor amet Lorem ipsum si dolor amet Lorem ipsum si dolor amet Lorem ipsum si dolor amet 
										 </p>
									</div>

									<div class="card-action">
									<a href="#" class="" id="editCamp"><i class="material-icons right" >edit</i></a>
									<a href="#" class=" " id="supprCamp"><i class="material-icons right">delete</i></a>
								</div>
								</div>
							</div>
						</div>

						<!-- Le bouton plus -->
						<div class="fixed-action-btn">
			  				<a class="btn-floating btn-large red darken-4" href="<?php echo site_url('AdminController/addPersonnel'); ?>">
			    				<i class="large material-icons" style="color: white">add</i>
			  				</a>
						</div>
					</div>
				</div>
</section>