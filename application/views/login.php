<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<DOCTYPE html>

<html>
<head>
	<title>CareSoft-Login</title>
	<!-- Import du css -->
	<link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/css/materialize.css'); ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/css/style.css'); ?>">

	<!-- Import du JS -->
	<script type="text/javascript" src="../js/materialize.js"></script>

	<!-- Import des material icon  -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

	<!-- Optimisation sur mobile -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body>
<!-- le header -->
	<header>
		<!-- Le nav -->
		<div class="navbar-fixed ">
			<nav>
				<div class="nav-wrapper red darken-4">
					<a href="<?php echo site_url('Welcome'); ?>"class="brand-logo center">La Grace</a>
				</div>
				
			</nav>
		</div> 
	</header>

	<!-- section -->
	<section class="section login-container">

		<div class="container">
			<div class="row">
				<form class="col s12 offset-m1 m10 offset-l4 l5  grey lighten-4" method="post" action="">
					<h6 class="center grey-text">Connectez vous ici ...</h6>
					<div style="color: red; ">
						<?php 
							echo $msgError['msge'];
						?>
					</div>
					<br>
					<div class="row">
						<div class="input-field col l12">
						<i class="material-icons prefix left">account_circle</i>
						<input class="validate" type="text" id="username" name="username" >
						<label for="username"> Nom d'utilisateur</label>
						<span class="helper-text" data-error="Veuillez remplir ce champ" ></span>
						<p style="color:red"><?php echo form_error('username'); ?></p>
					</div>
					</div>
					
					<br>
					<div class="input-field">
						<i class="material-icons prefix left">lock</i>
						<input class="valiadte" type="password" id="password" name="mdp">
						<label for="password"> Mot de passe</label>
						<?php echo form_error('mdp'); ?>
        			</div>
					
					<br>
					<div class="input-field">
						<div class="row">
							<div class="col s12 center">
								<button type="submit" class="btn red darken-4 waves-effect waves-light" value="Se connecter"><i class="material-icons left">input</i>Se connecter</button>
							</div>
						</div>
					</div>
					<div class="row">
						<div class=".col.offset-s4 .col.offset-l2 .col.offset-m4  col s8 l6 m4 ">
							
						</div>
						<a href="#" class="red-text text-darken-4">Mot de passe oublié ? </a>
					</div>
				</form>
			</div>
		</div>
	</section>

	<!-- Le footer -->
	<footer class="grey darken-4 page-footer">
		<div class="container">
			<div class="row">
				<div class="col s12 m5">
					<h5>Nous Contacter</h5>
					<div style="border-bottom: 2px solid red; width: 20%"></div>
					<p>Lorem ipsum Lorem ipsum si dolor amet Lorem ipsum si dolor amet Lorem ipsum si dolor amet Lorem ipsum si doloramet Lorem ipsum sit</p>

					<div class="reseaux-sociaux">
						<ul>
							<li><a href="#" class="grey-text text-lighten-1"><i class="fa fab-facebook">facebook</i></a></li>
							<li><a href="#" class="grey-text text-lighten-1"><i class="fab fa-whatsapp">whatsapp</i></a></li>
							<li><a href="#" class="grey-text text-lighten-1"><i class="fab fa-instagram">instagram</i></a></li>
						</ul>
					</div>
				</div>

				<div class="col s12 offset-m2 m5" >
					<h5>Politique du Centre</h5>
					<div style="border-bottom: 2px solid red; width: 20%"></div>
					<p>Lorem ipsum Lorem ipsum si dolor amet Lorem ipsum si dolor amet Lorem ipsum si dolor amet Lorem ipsum si doloramet Lorem ipsum sit</p>

					
				</div>
			</div>
		</div>

		<div class="footer-copyright grey darken-2">
			<div class="container">
				<p class="center-align">&copy Copyright CareSoft</p>
			</div>
			
		</div>
	</footer>

	<!-- Du JS et du Jquery  -->
	
	<script src="<?php echo site_url('assets/js/jquery.js'); ?>"></script>
	<script src="<?php echo site_url('assets/js/materialize.js'); ?>"></script>
	<script type="text/javascript">
		 $(document).ready(function() {
    		M.updateTextFields();
  		});
  	</script>
</body>
</html>

