<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<meta charset="utf-8">
<html>
<head>
	<title>Edit</title>
	<!-- Import du css -->
	<link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/css/materialize.css'); ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/css/style.css'); ?>">

	
	<!-- Import des material icon  -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

	<!-- Optimisation sur mobile -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body>
	<div class="container">
			<div class="row">
				<form class="col s12 offset-m4 m5 grey lighten-4">
					<div class="input-field">
						<input class="validate" type="text" id="medecinName" name="medecinName">
						<label for="medecinName"> Nom</label>
						<span class="helper-text" data-error="Veuillez remplir ce champ" data-success="Validé"></span>
					</div>
					<br>
					<div class="input-field">
						<input class="validate" type="text" id="medecinSpecialite" name="medecinSpecialite">
						<label for="medecinSpecialite"> Spécialité</label>
						<span class="helper-text" data-error="Veuillez remplir ce champ" data-success="Validé"></span>
					</div>
					<div class="input-field">
						<textarea id="medecinDispo" name="medecinDispo" ></textarea>
						<label for="medecinDispo">Une petite desciption ici...</label>
						<span class="helper-text" data-error="Veuillez remplir ce champ" data-success="Validé"></span>
        			</div>
					
					<br>
					<div class="input-field">
						<div class="row">
							<div class="col s12 center">
								<button type="submit" class="btn red darken-4 waves-effect waves-light" value="enregModifsPers">Enregistrer</button>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>

		<!-- Du JS et du Jquery  -->
	
	<script src="<?php echo site_url('assets/js/jquery.js'); ?>"></script>
	<script src="<?php echo site_url('assets/js/materialize.js'); ?>"></script>
	<script type="text/javascript">
		 $(document).ready(function() {
    		M.updateTextFields();
  		});
  	</script>
  	
</body>
</html>
