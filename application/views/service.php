<section class="white darken-4 col s8 m9 l10">
	<div class=" services">
					<h4 class="center red-text text-darken-4">Les services </h4>
					<div class="row">
						<?php foreach($serv as $servs): ?>
						<div class="col s12 m4 l4">
							<div class="card">
								<div class="card-image">
									<img src="<?php echo site_url('assets/image/computer.jpg'); ?>" alt="une image" class="responsive-img">
								</div>
								<div class="card-title center-align red-text text-darken-4 ">
									<h5><?php echo $servs->nomService; ?></h5>
								</div>
								<div class="card-content">
									<p><?php echo $servs->descService; ?> </p>
								</div>

								<div class="card-action">
									<a href="#" class=""><i class="material-icons right" >edit</i></a>
									<a href="#" class=" "><i class="material-icons right">delete</i></a>
								</div>

							</div>
						</div>

						<?php endforeach; ?>

					<!-- Le bouton plus -->
					<div class="fixed-action-btn">
			  			<a class="btn-floating btn-large red darken-4" href="<?php echo site_url('AdminController/addService'); ?>">
			    			<i class="large material-icons" style="color: white">add</i>
			  			</a>
					</div>
				</div>
		</section>
	</div>
	
</section>
</body>
</html>