<!-- Libraire font awesome pour les icones des reseaux sociaux  -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

<!-- Le footer -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<footer class="grey darken-4 page-footer">
		<div class="container">
			<div class="row">
				<div class="col s12 m5">
					<h5>Nous Contacter</h5>
					<div style="border-bottom: 2px solid red; width: 20%">
					</div>
					<p>Lorem ipsum Lorem ipsum si dolor amet Lorem ipsum si dolor amet Lorem ipsum si dolor amet Lorem ipsum si doloramet Lorem ipsum sit</p>

					<div class="reseaux-sociaux">
						
						<a href="#" class="btn-floating waves-effect waves-light btn-small grey darken-1 social-icon social"><i class="fa fa-facebook"></i></a></li>
						<a href="#" class="btn-floating btn-small waves-effect waves-light grey darken-1"><i class="fa fa-whatsapp"></i></a>
						<a href="#" class="btn-floating btn-small waves-effect waves-light grey darken-1"><i class="fa fa-instagram"></i></a>
					</div>
				</div>

				<div class="col s12 offset-m2 m5" >
					<h5>Politique du Centre</h5>
					<div style="border-bottom: 2px solid red; width: 20%"></div>
					<p>Lorem ipsum Lorem ipsum si dolor amet Lorem ipsum si dolor amet Lorem ipsum si dolor amet Lorem ipsum si doloramet Lorem ipsum sit</p>

					
				</div>
			</div>
		</div>

		<div class="footer-copyright grey darken-2">
			<div class="container">
				<p class="center-align">&copy Copyright CareSoft</p>
			</div>
			
		</div>
	</footer>

</body>
</html>