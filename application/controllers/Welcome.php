<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	// Le constructeur 
	public function __construct() {
        parent::__construct();
     

        // chargement des helper url et form
        $this->load->helper(array('form', 'url'));
         
        // Chargement de la librairie form_validation
        $this->load->library('form_validation');

        // Chargement des model
        $this->load->model('TemModel');
        $this->load->model('CabModel');
        $this->load->model('ServiceModel');
        $this->load->model('PersModel');


		
         
    }

	public function index() {
		
		// Une variable qui va contenir toutes les infos a afficher 
		$data = array();

		// Pour garder la liste des témoignages
		$data['tem'] = $this->TemModel->listTem();

		// Pour garder le mot de bienvenue, la vision et l'histoire histoire
		$data['cab'] = $this->CabModel->listCab();

		// Pour garder le nom du Cabinet
		$data['cabNom'] = $this->CabModel->nomCab();

		// Pour garder la liste des services 
		$data['serv'] = $this->ServiceModel->listService();

		// Pour garder la liste des membres du personnel
		$data['pers'] = $this->PersModel->listPers();
		
		$this->load->view('header', $data);
		$this->load->view('index', $data);
		$this->load->view('footer');
	}

	public function login(){

		// on load l'admin Model 
		$this->load->model('AdminModel');

		// Pour l'afficahge des erreurs 
		$error = array();

		$test  = array('msge' => "");
		$error['msgError'] = $test ;
		

		

		//	Chargement de la bibliothèque pour la validation du formulaire
		$this->load->library('form_validation');

		$username = $this->input->post('username');
		$mdp = $this->input->post('mdp');

		// Regles de validation du formulaire 
		$this->form_validation->set_rules('username', '"Nom d\'utilisateur"', 'trim|required');
		$this->form_validation->set_rules('mdp',    '"Mot de passe"',       'trim|required');

		$this->load->view('login', $error);

		//	Le formulaire est valide

		if ($this->form_validation->run()) {

			if ($mdp != null && $username != null) {
						$data =  array();

						$data['bool']= $this->AdminModel->isAdmin($mdp, $username);

						if($data['bool'] == true){

							var_dump($data['bool']);
					
							redirect('AdminController/index');

							$data['username'] = $username ;

							redirect('AdminController/index/'.$data, 'location');	
						}

						// Si les valeurs entrées par l'utilisateur ne correspondent a aucune données en bd
						else{
							

							
							//redirect('Welcome/login/'.$error, 'location');
							



							
						}
				}
		}
				
	}

	public function errorLogin(){
		// Pour l'afficahge des erreurs 
		$error = array();

		$test  = array('msge' => "Le nom d'utilisateur ou le mot de passe est incorrect");
		$error['msgError'] = $test ;
		
		
		
		$this->load->view('login', $error);
		$this->login();
	}

	public function sayHello(){
		echo "Hello";
	}
}
