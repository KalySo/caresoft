<?php
	
	defined('BASEPATH') OR exit('No direct script access allowed');
	class AdminController extends CI_Controller
	{

		public function __construct() 
    {
        parent::__construct();
     
        // load form and url helpers
        $this->load->helper(array('form', 'url'));
         
        // load form_validation library
        $this->load->library('form_validation');

          // Chargement des model
        $this->load->model('TemModel');
        $this->load->model('CabModel');
        $this->load->model('ServiceModel');
        $this->load->model('PersModel');

        // On load le header dans le controller
        /// Pour afficher le nom du cabinet 

		$data = array('cabNom' =>$this->CabModel->nomCab()); 

		$this->load->view('headerAdmin', $data);

    }
		
		public function index(){

			// Une variable qui va contenir toutes les infos a afficher 
		$data = array();

		// Pour garder la liste des témoignages
		$data['tem'] = $this->TemModel->listTem();

		// Pour garder le mot de bienvenue, la vision et l'histoire et le nom
		$data['cab'] = $this->CabModel->listCab();

		

		// Pour garder la liste des services 
		$data['serv'] = $this->ServiceModel->listService();

		// Pour garder la liste des membres du personnel
		$data['pers'] = $this->PersModel->listPers();

			$username = "KalySo";

			$this->session->set_userdata('username', $username);
			$this->load->view('admin', $data);
			$this->load->view('cab', $data);


			

		}

		//Fonction pour afficher la page index chex l'admin
		public function indexAdmin(){

			// Une variable qui va contenir toutes les infos a afficher 
		$data = array();

		// Pour garder la liste des témoignages
		$data['tem'] = $this->TemModel->listTem();

		// Pour garder le mot de bienvenue, la vision et l'histoire histoire
		$data['cab'] = $this->CabModel->listCab();

		// Pour garder la liste des services 
		$data['serv'] = $this->ServiceModel->listService();

		// Pour garder la liste des membres du personnel
		$data['pers'] = $this->PersModel->listPers();


			$this->load->view('index', $data);
			$this->load->view('footer');
		}

		//Fontion de deconnexion
		public function deconnect(){

			//	Détruit la session
			$this->session->sess_destroy();

			//	Redirige vers la page d'accueil
			redirect('Welcome/login');
		}

		// Fonction pour update les infos du cabinet 
		public function updateCab($id){
			$nomCab = $this->input->post('nomCab');
			$histoire = $this->input->post('histoire');
			$vision = $this->input->post('vision');
			$mot = $this->input->post('mmotBienvenue');

			

			
				//$this->CabModel->updateCab($id, $nomCab, $histoire, $vision, $mot);
				//$this->load->view('success');
				//$this->index;
			
		} 

		//Fonction pour afficher la liste des témoignages 
		public function listTem(){
			// Une variable qui va contenir toutes les infos a afficher 
		$data = array();

		// Pour garder la liste des témoignages
		$data['tem'] = $this->TemModel->listTem();

		
			$this->load->view('admin', $data);
			$this->load->view('temoignage', $data);
		}

		//Fonction pour afficher la liste des services 
		public function listService(){
			
		

		// Pour garder la liste des services 
		$data['serv'] = $this->ServiceModel->listService();

			$this->load->view('admin', $data);
			$this->load->view('service', $data);
		}

		//Fonction pour afficher la liste des articles
		public function listArt(){

			
			$this->load->view('admin');
			$this->load->view('article' );
		}

		//Fonction pour afficher la liste des membres du Personnel
		public function listPersonnel(){
			
			// Une variable qui va contenir toutes les infos a afficher 
		$data = array();

		// Pour garder la liste des témoignages
		$data['tem'] = $this->TemModel->listTem();

		// Pour garder le mot de bienvenue, la vision et l'histoire histoire
		$data['cab'] = $this->CabModel->listCab();

		// Pour garder la liste des services 
		$data['serv'] = $this->ServiceModel->listService();

		// Pour garder la liste des membres du personnel
		$data['pers'] = $this->PersModel->listPers();

			$this->load->view('admin', $data);
			$this->load->view('personnel', $data);
		}


		//Fonction pour afficher la liste des rendez vous
		public function listrdv(){
			
			// Une variable qui va contenir toutes les infos a afficher 
		$data = array();


			$this->load->view('admin');
			$this->load->view('rdv');
		}

		//Fonction pour afficher la liste des campagnes
		public function listCamp(){
			
			$this->load->view('admin');
			$this->load->view('campagne');
		}


		//Function pour ajouter un temoignage 
		public function editTem(){

			$this->load->view('admin');
			$this->load->view('editTerm');

			//	Chargement de la bibliothèque pour la validation du formulaire
			$this->load->library('form_validation');

			$temoinName = $this->input->post('temoinName');

			$temoignage = $this->input->post('temoignage');

			// Définition des rules pour form_validation
			//Définir la taille aussi
			if($temoinName != null && $temoignage != null){

				$this->form_validation->set_rules('temoinName', '"Nom"', 'trim|required|min_length[5]');
				$this->form_validation->set_rules('temoignage', '"Temoignage"', 'trim|required|min_length[20]', array('min_length'=>'Au moins 20 caracteres'));
				$this->form_validation->set_message('min_length', '{field} must have at least {param} characters.');

				if($this->form_validation->run()){
					$this->TemModel->addTem($temoinName,$temoignage);

					// on appelle la vue success

					$this->load->view('success');
				}

				else{
				
					// On appelle la vue echec					
					$this->load->view('echec');
				}				 
			}
			
		}

		// Fonction pour supprimer un temoignage
		public function deleteTem($id){

			//$this->TemModel->deleteTem($id);
			
			echo $id;
			$this->load->view('success');
			$this->index();
			
			
		}


		// nFonction pour mettre a jour un témoignage 
		public function updateTem($id){

			$temoinName = $this->input->post('temoinName');

			$temoignage = $this->input->post('temoignage');

			var_dump($temoinName);
			var_dump($temoignage);
			// Définition des rules pour form_validation
			//Définir la taille aussi
			if($temoinName != null && $temoignage != null){

					$this->TemModel->updateTem($id, $temoinName,$temoignage);

					// on appelle la vue success

					$this->load->view('success');
					$this->index();
			}

				else{
				
					// On appelle la vue echec					
					$this->load->view('echec');
					$this->index();
				}
		}	

		

		// Fonction pour ajouter un service 
		public function addService(){

			
			$this->load->view('admin');
			$this->load->view('editServices');

			//	Chargement de la bibliothèque pour la validation du formulaire
			$this->load->library('form_validation');

			$servName = $this->input->post('serviceName');

			$servDesc = $this->input->post('serviceDesc');

			// Définition des rules pour form_validation
			//Définir la taille aussi
			if($servDesc != null && $servName != null){

				$this->form_validation->set_rules('serviceName', '"Nom du service"', 'trim|required|min_length[5]');
				$this->form_validation->set_rules('serviceDesc', '"Une petite desciption ici..."', 'trim|required|min_length[20]');
				
				if($this->form_validation->run()){
					
					$this->ServiceModel->addServ($servName,$servDesc);

					// on appelle la vue success

					$this->load->view('success');
				}

				else{
				
					// On appelle la vue echec					
					$this->load->view('echec');
				}				 
			}
			
		}


		

		// Fonction pour ajouter le Personnel 
		public function addPersonnel(){
			
			$this->load->view('admin');
			$this->load->view('editPersonnel');
		}

		// Fonction pour ajouter une nouvelle Campagne 
		public function addCamp(){
			
			$this->load->view('admin');
			$this->load->view('editPersonnel');
		}

		// Fonction pour ajouter un nouvel article 
		public function addArt(){
		
			$this->load->view('admin');
			$this->load->view('editPersonnel');
		}
}
	
?>