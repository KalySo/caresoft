<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class CabModel extends CI_Model
{
	//Recuperer la table temoignage
	protected $table = 'cabinet';


	// Function pour afficher les infos propres au cabinet (vision, histoire, mot de bienvenue ) 
	public function listCab(){
		$this->db->select('*')
			  ->from($this->table);
			  
		$query = $this->db->get();

		return $query->first_row();
	}

	// Function pour afficher le nom du Cabinet 
	public function nomCab(){
		$this->db->select('nomCab')
			  ->from($this->table);
			  
		$query = $this->db->get();

		return $query->first_row();
	}

	// Fonction pour modifier des infos du cabinet  
	public function updateCab($id, $nomCab=null, $histoire=null, $vision=null, $mot=null){

		$this->db->set('nomCab', $nomCab);
		$this->db->set('histoire', $histoire);
		$this->db->set('vision', $vision);
		$this->db->set('motBienvenue', $mot);
		$this->db->where('idCab', $id);
		$this->db->update($this->table);
		
	}



	
}

?>