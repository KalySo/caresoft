<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class TemModel extends CI_Model
{
	//Recuperer la table temoignage
	protected $table = 'temoignage';


	// Function pour afficher la liste des témoignages
	public function listTem(){
		$this->db->select('*')
			  ->from($this->table);
			  
		$query = $this->db->get();

		return $query->result();
	}

	// Fonction pour ajouter un témoignage
	public function addTem($nomTem, $tem){
	
		$this->db->set('nomTem', $nomTem);
		$this->db->set('contenuTem', $tem);
		/*if ( ! $this->db->insert($this->table)){
        		$error = $this->db->error(); 
        		echo $error;
		}
		*/
		$this->db->insert($this->table);
	}


	// Fonction pour mettre un temoignage a jour 
	public function updateTem($id, $nomTem=null, $tem=null){

		$this->db->set('nomTem', $nomTem);
		$this->db->set('contenuTem', $tem);
		$this->db->where('idTem', $id);
		$this->db->update($this->table);
		
	}
	
	

	// Fonction pour supprimer un temoignage 
	public function deleteTem($id){
		$this->db->where('idTem', $id);
		$this->db->delete($this->table);
	}


	
}

?>