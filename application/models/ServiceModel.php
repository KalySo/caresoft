<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ServiceModel extends CI_Model
{
	//Recuperer la table services
	protected $table = 'services';


	// Function pour afficher la liste des services
	public function listService(){
		$this->db->select('*')
			  ->from($this->table);
			  
		$query = $this->db->get();

		return $query->result();
	}

	// Fonction pour ajouter un service
	public function addServ($nomServ, $descServ){
	
		$this->db->set('nomService', $nomServ);
		$this->db->set('descService', $descServ);
		/*if ( ! $this->db->insert($this->table)){
        		$error = $this->db->error(); 
        		echo $error;
		}
		*/
		$this->db->insert($this->table);
	}


	// Fonction pour supprimer un service 


	// Fonction pour modifier des infos sur un service

	
}

?>