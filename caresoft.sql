-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  sam. 23 mars 2019 à 07:12
-- Version du serveur :  5.7.24
-- Version de PHP :  5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `caresoft`
--

-- --------------------------------------------------------

--
-- Structure de la table `admin`
--

DROP TABLE IF EXISTS `admin`;
CREATE TABLE IF NOT EXISTS `admin` (
  `idAdmin` int(11) NOT NULL AUTO_INCREMENT,
  `nameAdmin` varchar(65) NOT NULL,
  `pswdAdmin` varchar(65) NOT NULL,
  PRIMARY KEY (`idAdmin`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `admin`
--

INSERT INTO `admin` (`idAdmin`, `nameAdmin`, `pswdAdmin`) VALUES
(1, 'KalySo', 'KalySo');

-- --------------------------------------------------------

--
-- Structure de la table `apropos`
--

DROP TABLE IF EXISTS `apropos`;
CREATE TABLE IF NOT EXISTS `apropos` (
  `idapropos` int(11) NOT NULL AUTO_INCREMENT,
  `vision` text,
  `histoire` text,
  `idAdmin` int(11) DEFAULT NULL,
  PRIMARY KEY (`idapropos`),
  KEY `idAdmin` (`idAdmin`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `apropos`
--

INSERT INTO `apropos` (`idapropos`, `vision`, `histoire`, `idAdmin`) VALUES
(1, 'Lorem ipsum Lorem ipsum si dolor amet Lorem ipsum si dolor amet Lorem ipsum si dolor amet Lorem ipsum si doloramet Lorem ipsum sit', 'Lorem ipsum Lorem ipsum si dolor amet Lorem ipsum si dolor amet Lorem ipsum si dolor amet Lorem ipsum si doloramet Lorem ipsum sit', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `article`
--

DROP TABLE IF EXISTS `article`;
CREATE TABLE IF NOT EXISTS `article` (
  `idArt` int(11) NOT NULL AUTO_INCREMENT,
  `idCatArt` int(11) DEFAULT NULL,
  `titreArt` varchar(100) NOT NULL,
  `contenuArt` text NOT NULL,
  `tof` text NOT NULL,
  PRIMARY KEY (`idArt`),
  KEY `idCatArt` (`idCatArt`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `cabinet`
--

DROP TABLE IF EXISTS `cabinet`;
CREATE TABLE IF NOT EXISTS `cabinet` (
  `idCab` int(11) NOT NULL AUTO_INCREMENT,
  `nomCab` varchar(50) NOT NULL DEFAULT 'La Grace',
  `histoire` text NOT NULL,
  `vision` text NOT NULL,
  `motBienvenue` text NOT NULL,
  `idAdmin` int(11) DEFAULT NULL,
  PRIMARY KEY (`idCab`),
  KEY `idAdmin` (`idAdmin`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `cabinet`
--

INSERT INTO `cabinet` (`idCab`, `nomCab`, `histoire`, `vision`, `motBienvenue`, `idAdmin`) VALUES
(2, 'La Grace', 'Lorem ipsum Lorem ipsum si dolor amet Lorem ipsum si dolor amet Lorem ipsum si dolor amLorem ipsum Lorem ipsum si dolor amet Lorem ipsum si dolor amet Lorem ipsum si dolor amet Lorem ipsum si dolor ametet Lorem ipsum si dolor amet', 'Lorem ipsum Lorem ipsum si dolor amet Lorem ipsum si dolor amet Lorem ipsum si dolor amet Lorem ipsum si dolor ametLorem ipsum Lorem ipsum si dolor amet Lorem ipsum si dolor amet Lorem ipsum si dolor amet Lorem ipsum si dolor amet', 'Ouvert tous les jours 24h/24<br>\r\nVotre santé est notre priorité', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `catart`
--

DROP TABLE IF EXISTS `catart`;
CREATE TABLE IF NOT EXISTS `catart` (
  `idCatArt` int(11) NOT NULL AUTO_INCREMENT,
  `nomCatArt` varchar(65) DEFAULT NULL,
  `idAdmin` int(11) NOT NULL,
  PRIMARY KEY (`idCatArt`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `commenter`
--

DROP TABLE IF EXISTS `commenter`;
CREATE TABLE IF NOT EXISTS `commenter` (
  `idComment` int(11) NOT NULL AUTO_INCREMENT,
  `idInternaute` int(11) NOT NULL,
  `comment` text NOT NULL,
  PRIMARY KEY (`idComment`),
  KEY `idInternaute` (`idInternaute`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `internaute`
--

DROP TABLE IF EXISTS `internaute`;
CREATE TABLE IF NOT EXISTS `internaute` (
  `idInternaute` int(11) NOT NULL AUTO_INCREMENT,
  `pseudo` varchar(100) NOT NULL,
  PRIMARY KEY (`idInternaute`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `personnel`
--

DROP TABLE IF EXISTS `personnel`;
CREATE TABLE IF NOT EXISTS `personnel` (
  `idMembre` int(11) NOT NULL AUTO_INCREMENT,
  `nomMembre` varchar(65) NOT NULL,
  `specialiteMembre` text NOT NULL,
  `disponibiliteMembre` text NOT NULL,
  `idAdmin` int(11) DEFAULT NULL,
  PRIMARY KEY (`idMembre`),
  KEY `idAdmin` (`idAdmin`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `personnel`
--

INSERT INTO `personnel` (`idMembre`, `nomMembre`, `specialiteMembre`, `disponibiliteMembre`, `idAdmin`) VALUES
(1, 'Helene Ngo', 'Cardiologue', 'kogrevgfrnibvdofidfvfrde', NULL),
(2, 'Arlette Mingan', 'Medecine Generale', 'rtyuifdtty6e7feyfhdshuyew', NULL),
(3, 'Albert Foka', 'Infirmier Supérieur\r\n	', 'De  Lundi a Vendredi: 7h30-11h \r\nSamedi et Dimanche: 17h - 20h30', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `services`
--

DROP TABLE IF EXISTS `services`;
CREATE TABLE IF NOT EXISTS `services` (
  `idService` int(11) NOT NULL AUTO_INCREMENT,
  `nomService` varchar(65) NOT NULL,
  `descService` text NOT NULL,
  `idAdmin` int(11) DEFAULT NULL,
  PRIMARY KEY (`idService`),
  KEY `idAdmin` (`idAdmin`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `services`
--

INSERT INTO `services` (`idService`, `nomService`, `descService`, `idAdmin`) VALUES
(1, 'Vaccination', 'Lorem ipsum Lorem ipsum si dolor amet Lorem ipsum si dolor amet Lorem ipsum si dolor amet Lorem ipsum si dolor amet', NULL),
(3, 'Accouchement', 'Lorem ipsum Lorem ipsum si dolor amet Lorem ipsum si dolor amet Lorem ipsum si dolor amet Lorem ipsum si dolor amet', NULL),
(4, 'Consultation pour medecine generale', 'Lorem ipsum Lorem ipsum si dolor amet Lorem ipsum si dolor amet Lorem ipsum si dolor amet Lorem ipsum si dolor amet', NULL),
(5, 'Faiblesse Sexuelle', 'Lorem ipsum Lorem ipsum si dolor amet Lorem ipsum si dolor amet Lorem ipsum si dolor amet Lorem ipsum si dolor amet', NULL),
(6, 'dsvgfds', 'fdgbvdfbgtyhuwvgedtgyegwvfd', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `temoignage`
--

DROP TABLE IF EXISTS `temoignage`;
CREATE TABLE IF NOT EXISTS `temoignage` (
  `idTem` int(11) NOT NULL AUTO_INCREMENT,
  `nomTem` varchar(65) NOT NULL,
  `contenuTem` text NOT NULL,
  `idAdmin` int(11) DEFAULT NULL,
  PRIMARY KEY (`idTem`),
  KEY `idAdmin` (`idAdmin`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `temoignage`
--

INSERT INTO `temoignage` (`idTem`, `nomTem`, `contenuTem`, `idAdmin`) VALUES
(5, 'Kengza', 'Cabinet trdfghjkl;mmnbvhoiuytsghbjnmk fghjk\r\nvfbgnjmkl;fd\r\nddfffkmjhsdhdbhd\r\nvbnm,\r\ndfgthyjukiolp\r\nrftgyhujkiol\r\nfghyjukilo\r\nfgthyjukilo\r\n', NULL),
(6, 'Bonjour', 'On se sent en sécurité et bien dans sa peau lorsqu\'on se trouve dans ces murs', NULL),
(7, 'Sorelle Kana', 'Lorem ipsum Lorem ipsum si dolor amet Lorem ipsum si dolor amet Lorem ipsum si dolor amet Lorem ipsum si dolor ametLorem ipsum Lorem ipsum si dolor amet Lorem ipsum si dolor amet Lorem ipsum si dolor amet Lorem ipsum si dolor amet', NULL),
(8, 'Arielle Kitio', 'Lorem ipsum Lorem ipsum si dolor amet Lorem ipsum si dolor amet Lorem ipsum si dolor amet Lorem ipsum si dolor ametLorem ipsum Lorem ipsum si dolor amet Lorem ipsum si dolor amet Lorem ipsum si dolor amet Lorem ipsum si dolor amet', NULL);

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `apropos`
--
ALTER TABLE `apropos`
  ADD CONSTRAINT `apropos_ibfk_1` FOREIGN KEY (`idAdmin`) REFERENCES `admin` (`idAdmin`);

--
-- Contraintes pour la table `article`
--
ALTER TABLE `article`
  ADD CONSTRAINT `article_ibfk_1` FOREIGN KEY (`idCatArt`) REFERENCES `catart` (`idCatArt`);

--
-- Contraintes pour la table `cabinet`
--
ALTER TABLE `cabinet`
  ADD CONSTRAINT `cabinet_ibfk_1` FOREIGN KEY (`idAdmin`) REFERENCES `admin` (`idAdmin`);

--
-- Contraintes pour la table `personnel`
--
ALTER TABLE `personnel`
  ADD CONSTRAINT `personnel_ibfk_1` FOREIGN KEY (`idAdmin`) REFERENCES `admin` (`idAdmin`);

--
-- Contraintes pour la table `services`
--
ALTER TABLE `services`
  ADD CONSTRAINT `services_ibfk_1` FOREIGN KEY (`idAdmin`) REFERENCES `admin` (`idAdmin`);

--
-- Contraintes pour la table `temoignage`
--
ALTER TABLE `temoignage`
  ADD CONSTRAINT `temoignage_ibfk_1` FOREIGN KEY (`idAdmin`) REFERENCES `admin` (`idAdmin`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
